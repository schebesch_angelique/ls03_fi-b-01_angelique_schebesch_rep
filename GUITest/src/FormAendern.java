import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.colorchooser.DefaultColorSelectionModel;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.SwingConstants;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class FormAendern extends JFrame {

	private JPanel contentPane;
	private JTextField txtHierBitteText;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FormAendern frame = new FormAendern();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FormAendern() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 383, 585);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblText = new JLabel("Dieser Text soll ver\u00E4ndert werden");
		lblText.setHorizontalAlignment(SwingConstants.CENTER);
		lblText.setBounds(5, 5, 359, 71);
		contentPane.add(lblText);
		
		JLabel lblA1 = new JLabel("Aufgabe 1: Hintergrund \u00E4ndern");
		lblA1.setBounds(5, 86, 200, 13);
		contentPane.add(lblA1);
		
		JButton btnRot = new JButton("Rot");
		btnRot.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				contentPane.setBackground(Color.RED);
			}
		});
		btnRot.setBounds(5, 109, 99, 21);
		contentPane.add(btnRot);
		
		JButton btnGrn = new JButton("Gr\u00FCn");
		btnGrn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				contentPane.setBackground(Color.green);
			}
		});
		btnGrn.setBounds(136, 109, 99, 21);
		contentPane.add(btnGrn);
		
		JButton btnBlau = new JButton("Blau");
		btnBlau.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				contentPane.setBackground(Color.BLUE);
			}
		});
		btnBlau.setBounds(265, 109, 99, 21);
		contentPane.add(btnBlau);
		
		JButton btnGelb = new JButton("Gelb");
		btnGelb.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				contentPane.setBackground(Color.YELLOW);
			}
		});
		btnGelb.setBounds(5, 143, 99, 21);
		contentPane.add(btnGelb);
		
		JButton btnStandardfarbe = new JButton("Standardfarbe");
		btnStandardfarbe.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnStandardfarbe.setBounds(116, 143, 119, 21);
		contentPane.add(btnStandardfarbe);
		
		JButton btnFarbeWhlen = new JButton("Farbe w\u00E4hlen");
		btnFarbeWhlen.setBounds(247, 143, 117, 21);
		contentPane.add(btnFarbeWhlen);
		
		JLabel lblA2 = new JLabel("Aufgabe 2: Text formatieren");
		lblA2.setBounds(5, 174, 160, 13);
		contentPane.add(lblA2);
		
		JButton btnArial = new JButton("Arial");
		btnArial.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblText.setFont(new Font("Arial", Font.PLAIN, 11));
			}
		});
		btnArial.setBounds(5, 197, 77, 26);
		contentPane.add(btnArial);
		
		JButton btnComicSansMs = new JButton("Comic Sans MS");
		btnComicSansMs.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblText.setFont(new Font("Comic Sans MS", Font.PLAIN, 11));
			}
		});
		btnComicSansMs.setBounds(94, 197, 141, 26);
		contentPane.add(btnComicSansMs);
		
		JButton btnCourierNew = new JButton("Courier New");
		btnCourierNew.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblText.setFont(new Font("Courier New", Font.PLAIN, 11));
			}
		});
		btnCourierNew.setBounds(247, 197, 117, 26);
		contentPane.add(btnCourierNew);
		
		txtHierBitteText = new JTextField();
		txtHierBitteText.setText("Hier bitte Text eingeben");
		txtHierBitteText.setBounds(5, 235, 359, 20);
		contentPane.add(txtHierBitteText);
		txtHierBitteText.setColumns(10);
		
		JButton btnInsLabelSchreiben = new JButton("Ins Label schreiben");
		btnInsLabelSchreiben.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblText.setText(txtHierBitteText.getText());
			}
		});
		btnInsLabelSchreiben.setBounds(5, 267, 178, 26);
		contentPane.add(btnInsLabelSchreiben);
		
		JButton btnTextImLabel = new JButton("Text im Label löschen");
		btnTextImLabel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblText.setText(lblText.getName());
			}
		});
		btnTextImLabel.setBounds(195, 267, 169, 26);
		contentPane.add(btnTextImLabel);
		
		JLabel lblA3 = new JLabel("Aufgabe 3: Schriftfarbe \u00E4ndern");
		lblA3.setBounds(5, 305, 200, 16);
		contentPane.add(lblA3);
		
		JButton btnRot_1 = new JButton("Rot");
		btnRot_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblText.setForeground(Color.RED);
			}
		});
		btnRot_1.setBounds(5, 333, 99, 26);
		contentPane.add(btnRot_1);
		
		JButton btnBlau_1 = new JButton("Blau");
		btnBlau_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblText.setForeground(Color.BLUE);
			}
		});
		btnBlau_1.setBounds(136, 333, 99, 26);
		contentPane.add(btnBlau_1);
		
		JButton btnSchwarz = new JButton("Schwarz");
		btnSchwarz.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblText.setForeground(Color.BLACK);
			}
		});
		btnSchwarz.setBounds(265, 333, 99, 26);
		contentPane.add(btnSchwarz);
		
		JLabel lblNewLabel = new JLabel("Aufgabe 4: Schriftgr\u00F6\u00DFe \u00E4ndern");
		lblNewLabel.setBounds(5, 371, 200, 16);
		contentPane.add(lblNewLabel);
		
		JButton btnPlus = new JButton("+");
		btnPlus.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblText.setFont(new Font(getFont().getName(), getFont().getStyle(), getFont().getSize()+1));
			}
		});
		btnPlus.setBounds(5, 399, 169, 26);
		contentPane.add(btnPlus);
		
		JButton btnminus = new JButton("-");
		btnminus.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblText.setFont(new Font(getFont().getName(), getFont().getStyle(), getFont().getSize()-1));
			}
		});
		btnminus.setBounds(195, 399, 169, 26);
		contentPane.add(btnminus);
		
		JLabel lblNewLabel_1 = new JLabel("Aufgabe 5: Textausrichtung");
		lblNewLabel_1.setBounds(5, 437, 200, 16);
		contentPane.add(lblNewLabel_1);
		
		JButton btnlinks = new JButton("linksbündig");
		btnlinks.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblText.setHorizontalAlignment(SwingConstants.LEFT);
			}
		});
		btnlinks.setBounds(5, 465, 117, 26);
		contentPane.add(btnlinks);
		
		JButton btnZentriert = new JButton("zentriert");
		btnZentriert.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblText.setHorizontalAlignment(SwingConstants.CENTER);
			}
		});
		btnZentriert.setBounds(132, 465, 99, 26);
		contentPane.add(btnZentriert);
		
		JButton btnRechtsbndig = new JButton("rechtsbündig");
		btnRechtsbndig.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblText.setHorizontalAlignment(SwingConstants.RIGHT);
			}
		});
		btnRechtsbndig.setBounds(247, 465, 117, 26);
		contentPane.add(btnRechtsbndig);
		
		JLabel lblNewLabel_2 = new JLabel("Aufgabe 6: Programm beenden");
		lblNewLabel_2.setBounds(5, 495, 178, 16);
		contentPane.add(lblNewLabel_2);
		
		JButton btnexit = new JButton("Exit");
		btnexit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		btnexit.setBounds(5, 522, 359, 43);
		contentPane.add(btnexit);
	}
}
